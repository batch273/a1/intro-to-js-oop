/*

	1. How do you create arrays in JS?
		*by enclosing a comma-separated list of values inside square brackets []
	
	2. How do you access the first character of an array?
		*we can use the square bracket notation and specify the index of the first element, which is 0.

	3. How do you access the last character of an array?
		*we can use the square bracket notation and specify the index of the last element, which is the length of the array minus 1. 
	
	4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
		*indexOf()

	5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
		*forEach()

	6. What array method creates a new array with elements obtained from a user-defined function?
		*map()

	7. What array method checks if all its elements satisfy a given condition?
		*every()
	
	8. What array method checks if at least one of its elements satisfies a given condition?
		*some()

	9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
		*false

	10. True or False: array.slice() copies elements from original array and returns them as a new array.
		*true



*/	


let students = ["John", "Joe", "Jane", "Jessie"]

	function addToEnd(arr, str) {
  		if (typeof str !== "string") {
    		return "error - can only add strings to an array";
  		}

  			arr.push(str);
  			return arr;
		}

	function addToStart(arr, str) {
  		if (typeof str !== "string") {
    		return "error - can only add strings to an array";
  		}

  			arr.unshift(str);
  			return arr;
		}

	function elementChecker(arr, name) {
	  	if (arr.length === 0) {
	    	return "error-passed in array is empty";
	  	}
	  
	  	for (let i = 0; i < arr.length; i++) {
	    	if (arr[i] === name) {
	     	return true;
	    	}
	  	}
	  
	  	return false;
	}

	function checkAllStringsEnding(arr, char) {
	 	 if (arr.length === 0) {
	    	return "error - array must NOT be empty";
	  	}

	  	if (!arr.every((element) => typeof element === "string")) {
	    	return "error - all array elements must be strings";
	  	}

	  	if (typeof char !== "string") {
	   	 	return "error - 2nd argument must be of data type string";
	  	}

	  	if (char.length !== 1) {
	   		return "error - 2nd argument must be a single character";
	 	}

	  	return arr.every((element) => element.endsWith(char));
	}

	function stringLengthSorter(arr) {
	  	
	  	// Check if all elements are strings
	  	for (let i = 0; i < arr.length; i++) {
	    	if (typeof arr[i] !== 'string') {
	      		return 'error - all array elements must be strings';
	    	}
	  	}
	  
	  	// Sort the array based on string length
	  	arr.sort((a, b) => a.length - b.length);

	  		return arr;
	}

	function startsWithCounter(arr, char) {
	  	// Check if array is empty
	  	if (arr.length === 0) {
	    	return "error - array must NOT be empty";
	  	}

	  	// Check if all elements are strings
	  	if (!arr.every((elem) => typeof elem === "string")) {
	    	return "error - all array elements must be strings";
	  	}

	  	// Check if char argument is a string with length 1
	  	if (typeof char !== "string" || char.length !== 1) {
	    	return "error - 2nd argument must be a single character";
	  	}

	  	// Count elements starting with char argument
	  	const count = arr.filter((elem) =>
	    	elem.toLowerCase().startsWith(char.toLowerCase())
	  		).length;

	  	return count;
	}

	function likeFinder(arr, searchString) {
	  	// Check if array is empty
	  	if (arr.length === 0) {
	    	return "error - array must NOT be empty";
	  	}

	  	// Check if all elements in array are strings
	  	for (let i = 0; i < arr.length; i++) {
	    	if (typeof arr[i] !== "string") {
	      		return "error - all array elements must be strings";
	    	}
	  	}

	  	// Check if searchString is a string
	  	if (typeof searchString !== "string") {
	    	return "error - 2nd argument must be of data type string";
	  	}

	  	// Create a new array to hold matching elements
	  	let matchingElements = [];

	  	// Loop through array and add matching elements to new array
	  	for (let i = 0; i < arr.length; i++) {
	    
	    // Check if element contains searchString (case-insensitive)
	    	if (arr[i].toLowerCase().includes(searchString.toLowerCase())) {
	      matchingElements.push(arr[i]);
	    	}
	  	}

	  	// Return the new array of matching elements
	  	return matchingElements;
	}

	function randomPicker(arr) {
	  	// Check if the array is empty
	  	if (arr.length === 0) {
	    	return "error - array must NOT be empty";
	  	}

	  	// Generate a random index based on the length of the array
	  	const randomIndex = Math.floor(Math.random() * arr.length);

	  	// Return the element at the randomly generated index
	  	return arr[randomIndex];
	}

